<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[on}XMtkBN?)V5Uf6k[x<`60f<(Z?6B!Kz`VQ~)`t1NZA8%mL|KO,eX=6a#aX9](' );
define( 'SECURE_AUTH_KEY',  'b*X6LZy~V@VL,%Quv]4*/8v=3fL ,#Rl3r:Zl#Uam:W;V>k(<VZMEf%eqf9a>(Z:' );
define( 'LOGGED_IN_KEY',    '-sYX<cxe=JD$U[,:q@X>,18?l%,+8uqQ=H=9t-.#1{)Y1M*Dwz2RD`ViusOF|Le_' );
define( 'NONCE_KEY',        '$/S Q?/N1XV}C16/YsR?4ALTj#*EL2f%~RzAW/UvE>#h3Q>USrRhG!.xB&]AXNGT' );
define( 'AUTH_SALT',        'b^kLyyI_UHd:{ud#=QcRQ_{dOZZ`6tQ?Mq4m%Eg)zV?NUZsW6M8>cOuz:rYY]wWK' );
define( 'SECURE_AUTH_SALT', 'qJ+*S%Q+u/}-w<,-P$c<fdkf34PG F$6WR7lAQN9F}IMFe5UF( gh5OsBmlqU Vh' );
define( 'LOGGED_IN_SALT',   'N[vzIip`hrEmTK4sCms>U] -wcpOhs3_`hyktHQm.)IOeOyOC>;JCC454Td*e#sb' );
define( 'NONCE_SALT',       'EFF{z]8xY&9R0+`;1&k+IIlfE5u+@6lWVYJ_^]Nd;tP&1%wlaU3:v%i<)jz|D8b!' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
